package com.abbott.adc.service.businessevent.util;

import java.sql.Timestamp;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.abbott.adc.service.businessevent.constants.EventConstants;
import com.abbott.adc.service.businessevent.model.Env;
import com.abbott.adc.service.businessevent.model.Execution;
import com.abbott.adc.service.businessevent.model.Log;
import com.abbott.adc.service.businessevent.model.LogStructure;
import com.abbott.adc.service.businessevent.model.UseCase;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is used as a utility for creating logs in desired format
 * @author rjoshi4
 *
 */
@Component
public class LoggingUtil {
	
	@Autowired
	ObjectMapper mapper;
	
	Log log =null;
	/**
	 * 
	 * @param message
	 * @param eventOriginationSystem
	 * @param payload
	 * @param correlationId
	 * @return
	 * @throws JsonProcessingException
	 * 
	 *  This Method creates a log as per the required format
	 */
	
	public String createLogs(String message,String eventOriginationSystem,String payload, String correlationId) throws JsonProcessingException {
		
		log = new Log();
		LogStructure logStructure = new LogStructure();
		Execution execution = new Execution();
		UseCase useCase = new UseCase();
		Env env = new Env();
		
		env.setEnviornmentId(System.getenv("env"));
		env.setiP("");
		env.setServerName("");
		
		execution.setLoggingMessage(message);
		execution.setLogID(String.valueOf(UUID.randomUUID()));
		execution.setTimestamp(String.valueOf(new Timestamp(System.currentTimeMillis())));
		
		if (!correlationId.isEmpty()) {
			useCase.setCorelationID(String.valueOf(correlationId));
		}else {			
			useCase.setCorelationID(String.valueOf(UUID.randomUUID()));
		}
		useCase.setSourceName(eventOriginationSystem);
		useCase.setInterfaceName(EventConstants.INTERFACE.getVal());
		useCase.setTargetName(EventConstants.TARGETNAME.getVal());
		
		logStructure.setEnv(env);
		logStructure.setExecution(execution);
		logStructure.setUseCase(useCase);
		logStructure.setPayload(payload);
		log.setLogStructure(logStructure);
		
		return mapper.writeValueAsString(log);
	}

}
