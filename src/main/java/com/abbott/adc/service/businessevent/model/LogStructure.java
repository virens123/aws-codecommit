package com.abbott.adc.service.businessevent.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class containing log structure for logging
 *
 */

public class LogStructure {

	@JsonProperty("Env")
	private Env env;
	@JsonProperty("UseCase")
	private UseCase useCase;
	@JsonProperty("Execution")
	private Execution execution;
	@JsonProperty("Payload")
	private String payload;
	@JsonProperty("Exception")
	private LoggingException exception;
	
	public Env getEnv() {
		return env;
	}
	public void setEnv(Env env) {
		this.env = env;
	}
	public UseCase getUseCase() {
		return useCase;
	}
	public void setUseCase(UseCase useCase) {
		this.useCase = useCase;
	}
	public Execution getExecution() {
		return execution;
	}
	public void setExecution(Execution execution) {
		this.execution = execution;
	}
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	public void setException(LoggingException exception) {
		this.exception = exception;
	}
	public LoggingException getException() {
		return exception;
	}
	
	
}
