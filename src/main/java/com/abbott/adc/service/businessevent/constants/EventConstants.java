package com.abbott.adc.service.businessevent.constants;

/**
 * Enum contains the constants which are to be accessed across the application
 */
public enum EventConstants {

	TOPICPREFIX("ABBOTT.ADC."), TOPICSUFFIX("TOPIC"), INTERFACE("adc-services-businessevent"), TARGETNAME(
			"adc-b2c-webshop-adapter");

	/**
	 * The value of the enum in String.
	 * 
	 */
	private String val;

	EventConstants(String val) {
		this.val = val;
	}

	public String getVal() {
		return this.val;
	}

}