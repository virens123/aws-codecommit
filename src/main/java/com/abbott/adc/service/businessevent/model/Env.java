package com.abbott.adc.service.businessevent.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * class containing environment details for logging
 *
 */
public class Env {
	
	@JsonProperty("EnviornmentId")
	private String enviornmentId;
	@JsonProperty("IP")
	private String iP;
	@JsonProperty("serverName")
	private String serverName;
	public String getEnviornmentId() {
		return enviornmentId;
	}
	public void setEnviornmentId(String enviornmentId) {
		this.enviornmentId = enviornmentId;
	}
	public String getiP() {
		return iP;
	}
	public void setiP(String iP) {
		this.iP = iP;
	}
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	
	

}
