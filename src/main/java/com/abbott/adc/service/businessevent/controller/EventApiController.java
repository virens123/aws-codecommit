package com.abbott.adc.service.businessevent.controller;

import javax.jms.JMSException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abbott.adc.service.businessevent.model.Event;
import com.abbott.adc.service.businessevent.publisher.EventPublisher;
import com.abbott.adc.service.businessevent.util.EventTopicUtil;
import com.abbott.adc.service.businessevent.util.LoggingUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Controller to consume events from CSR
 */
@RestController
@RequestMapping(value = "/{country}/{language}/v1.0")
public class EventApiController {

	@Autowired
	EventPublisher eventPublisher;

	@Autowired
	ObjectMapper mapper;

	@Autowired
	LoggingUtil loggingUtil;
	
	@Autowired
	EventTopicUtil eventTopicUtil;

	private static final Logger logger = LogManager.getLogger(EventApiController.class);

	/**
	 * 
	 * @param country
	 * @param language
	 * @param consumerKey
	 * @param accessToken
	 * @param eventDomain
	 * @param eventType
	 * @param eventSubType
	 * @param eventOriginationSystem
	 * @param event
	 * @throws JsonProcessingException
	 * @throws Exception
	 *             Method to consume the events and process them to publish in
	 *             Topic.
	 */
	@PostMapping(value = "/events", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> processEvents(@PathVariable(value = "country") String country,
			@PathVariable(value = "language") String language, @RequestHeader(value = "consumerKey") String consumerKey,
			@RequestHeader(value = "accessToken") String accessToken,
			@RequestHeader(value = "eventDomain") String eventDomain,
			@RequestHeader(value = "eventType") String eventType,
			@RequestHeader(value = "eventSubType") String eventSubType,
			@RequestHeader(value = "eventOriginationSystem") String eventOriginationSystem, @RequestBody Event event)
			throws JsonProcessingException {

		logger.info(loggingUtil.createLogs("entering EventApiController", eventOriginationSystem, event.toString(),event.getEventCorrelationID()));

		eventPublisher.send(eventTopicUtil.createTopic(eventOriginationSystem, eventType), event, country, language, eventSubType, consumerKey, eventOriginationSystem,eventDomain,eventType);
		
		logger.info(loggingUtil.createLogs("exiting EventApiController", eventOriginationSystem, event.toString(),event.getEventCorrelationID()));
		return ResponseEntity.ok(HttpStatus.OK);
	}

}
