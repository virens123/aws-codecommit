package com.abbott.adc.service.businessevent.publisher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.abbott.adc.service.businessevent.model.Event;
import com.abbott.adc.service.businessevent.util.LoggingUtil;
import com.fasterxml.jackson.core.JsonProcessingException;


/**
 * A simple message producer which publishes the message to the JMS topic.
 */

@Component
public class EventPublisher {
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	LoggingUtil loggingUtil;

	private static final Logger logger = LogManager.getLogger(EventPublisher.class);

	/**
	 * 
	 * @param topic
	 * @param event
	 * @param country
	 * @param language
	 * @param eventSubType
	 * @param consumerKey
	 * @param eventOriginationSystem
	 * @param eventDomain
	 * @param eventType
	 * Method to convert and set the header parameters so as to publish the event into topic
	 * @throws JsonProcessingException 
	 */
	public void send(String topic, Event event, String country, String language, String eventSubType, String consumerKey,String eventOriginationSystem, String eventDomain, String eventType) throws JsonProcessingException {
		logger.debug(loggingUtil.createLogs("entering EventPublisher" + topic ,eventOriginationSystem ,event.toString(),event.getEventCorrelationID()));
		jmsTemplate.convertAndSend(topic, event, message -> {
			
			// setting custom JMS headers before sending
			message.setStringProperty("country", country);
			message.setStringProperty("language", language);
			message.setStringProperty("eventSubType", eventSubType);
			message.setStringProperty("consumerKey", consumerKey);
			message.setStringProperty("eventDomain", eventDomain);
			message.setStringProperty("eventType", eventType);
			message.setStringProperty("JMSXGroupID",event.getMessageGroupID());
			return message;
		});
		logger.debug(loggingUtil.createLogs("exiting EventPublisher" + topic,eventOriginationSystem,event.toString(),event.getEventCorrelationID()));
	}
}