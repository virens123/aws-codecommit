package com.abbott.adc.service.businessevent.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class containing use case details for logging
 *
 */
public class UseCase {
	
	@JsonProperty("CorelationID")
	private String corelationID;
	@JsonProperty("SourceName")
	private String sourceName;
	@JsonProperty("TargetName")
	private String targetName;
	@JsonProperty("InterfaceName")
	private String interfaceName;
	public String getCorelationID() {
		return corelationID;
	}
	public void setCorelationID(String corelationID) {
		this.corelationID = corelationID;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public String getTargetName() {
		return targetName;
	}
	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}
	public String getInterfaceName() {
		return interfaceName;
	}
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}
	
	

}
