package com.abbott.adc.service.businessevent.util;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.appender.AppenderLoggingException;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.logs.AWSLogs;
import com.amazonaws.services.logs.AWSLogsClientBuilder;
import com.amazonaws.services.logs.model.CreateLogGroupRequest;
import com.amazonaws.services.logs.model.CreateLogStreamRequest;
import com.amazonaws.services.logs.model.DataAlreadyAcceptedException;
import com.amazonaws.services.logs.model.DescribeLogGroupsRequest;
import com.amazonaws.services.logs.model.DescribeLogStreamsRequest;
import com.amazonaws.services.logs.model.InputLogEvent;
import com.amazonaws.services.logs.model.InvalidSequenceTokenException;
import com.amazonaws.services.logs.model.LogGroup;
import com.amazonaws.services.logs.model.PutLogEventsRequest;
import com.amazonaws.services.logs.model.PutLogEventsResult;

/**
 * Class used to read the logs from application and push that logs into
 * cloud-watch.
 * 
 * @author sshendge
 *
 */
@Plugin(name = "CustomLog4jAppender", category = "Core", elementType = "appender", printObject = true)
public class CustomLog4jAppender extends AbstractAppender {
	private final Boolean DEBUG_MODE = System.getProperty("log4j.debug") != null;

	/**
	 * Used to make sure that on close() our daemon thread isn't also trying to
	 * sendMessage()s
	 */
	private Object sendMessagesLock = new Object();

	/**
	 * The queue used to buffer log entries
	 */
	private LinkedBlockingQueue<LogEvent> loggingEventsQueue;

	/**
	 * the AWS Cloudwatch Logs API client
	 */
	private AWSLogs awsLogsClient;

	private AtomicReference<String> lastSequenceToken = new AtomicReference<>();

	/**
	 * The AWS region
	 */
	private Optional<String> region = Optional.empty();

	/**
	 * The queue / buffer size
	 */
	private static int queueLength = 1024;

	/**
	 * The maximum number of log entries to send in one go to the AWS Cloudwatch Log
	 * service
	 */
	private static int messagesBatchSize = 128;

	private static String logGroup;
	private static String logStream;

	/**
	 * True if the cloudwatch appender resources have been correctly initialised
	 */
	private AtomicBoolean cloudwatchAppenderInitialised = new AtomicBoolean(false);

	/**
	 * Used to keep the daemon thread alive.
	 */
	private AtomicBoolean keepDaemonActive = new AtomicBoolean(false);

	protected CustomLog4jAppender(String name, Filter filter, Layout<? extends Serializable> layout,
			final boolean ignoreExceptions) {
		super(name, filter, layout, ignoreExceptions);
	}

	@PluginFactory
	public static CustomLog4jAppender createAppender(@PluginAttribute("name") String name,
			@PluginElement("Layout") Layout<? extends Serializable> layout,
			@PluginElement("Filter") final Filter filter, @PluginAttribute("otherAttribute") String otherAttribute,
			@PluginAttribute("logGroupName") String logGroupName,
			@PluginAttribute("logStreamName") String logStreamName,
			@PluginAttribute("queueLengthValue") int queueLengthValue,
			@PluginAttribute("messagesBatchSizeValue") int messagesBatchSizeValue) {
		if (name == null) {
			LOGGER.error("No name provided for MyCustomAppenderImp");
			return null;
		}
		if (logGroupName != null) {
			logGroup = logGroupName;
		}
		if (logStreamName != null) {
			logStream = logStreamName;
		}

		if (queueLengthValue != 0) {
			queueLength = queueLengthValue;
		}

		if (messagesBatchSizeValue != 0) {
			messagesBatchSize = messagesBatchSizeValue;
		}

		if (layout == null) {
			layout = PatternLayout.createDefaultLayout();
		}
		return new CustomLog4jAppender(name, filter, layout, true);

	}

	/**
	 * Read and add all application logs into logging event queue.
	 * 
	 * @param event
	 */
	@Override
	public void append(LogEvent event) {
		new StringBuilder(event.getMessage().getFormattedMessage());
		try {
			if (cloudwatchAppenderInitialised.get()) {
				loggingEventsQueue.offer(event);
			} else {
			}
		} catch (Exception ex) {
			if (!ignoreExceptions()) {
				throw new AppenderLoggingException(ex);
			}
		}
	}

	/**
	 * This method actually used for to initialized and create the all resources
	 * which required for cloud-watch.
	 * 
	 */
	@Override
	public void start() {
		super.start();
		this.activateOptions();
	}

	@Override
	public void stop() {
		this.close();
		super.stop();
	}

	/**
	 * Send the message into cloud-watch which present into logging event queue.
	 * 
	 */
	private void sendMessages() {
		synchronized (sendMessagesLock) {
			LogEvent polledLoggingEvent;
			List<LogEvent> loggingEvents = new ArrayList<>();

			try {

				while ((polledLoggingEvent = loggingEventsQueue.poll()) != null
						&& loggingEvents.size() <= messagesBatchSize) {
					loggingEvents.add(polledLoggingEvent);
				}

				List<InputLogEvent> inputLogEvents = loggingEvents.stream()
						.map(loggingEvent -> new InputLogEvent().withTimestamp(loggingEvent.getTimeMillis())
								.withMessage(loggingEvent.getMessage().toString()))
						.sorted(comparing(InputLogEvent::getTimestamp)).collect(toList());

				if (!inputLogEvents.isEmpty()) {

					PutLogEventsRequest putLogEventsRequest = new PutLogEventsRequest(logGroup, logStream,
							inputLogEvents);

					try {
						putLogEventsRequest.setSequenceToken(lastSequenceToken.get());
						PutLogEventsResult result = awsLogsClient.putLogEvents(putLogEventsRequest);
						lastSequenceToken.set(result.getNextSequenceToken());
					} catch (DataAlreadyAcceptedException dataAlreadyAcceptedExcepted) {
						putLogEventsRequest.setSequenceToken(dataAlreadyAcceptedExcepted.getExpectedSequenceToken());
						PutLogEventsResult result = awsLogsClient.putLogEvents(putLogEventsRequest);
						lastSequenceToken.set(result.getNextSequenceToken());
						if (DEBUG_MODE) {
							dataAlreadyAcceptedExcepted.printStackTrace();
						}
					} catch (InvalidSequenceTokenException invalidSequenceTokenException) {
						putLogEventsRequest.setSequenceToken(invalidSequenceTokenException.getExpectedSequenceToken());
						PutLogEventsResult result = awsLogsClient.putLogEvents(putLogEventsRequest);
						lastSequenceToken.set(result.getNextSequenceToken());
						if (DEBUG_MODE) {
							invalidSequenceTokenException.printStackTrace();
						}
					}
				}
			} catch (Exception e) {
				if (DEBUG_MODE) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Initialize the cloud-watch resources based on Group name and log stream. Also
	 * if log stream and group name is not created in cloud-watch then created the
	 * same into cloud-watch.
	 * 
	 */
	private void initializeCloudwatchResources() {

		DescribeLogGroupsRequest describeLogGroupsRequest = new DescribeLogGroupsRequest();
		describeLogGroupsRequest.setLogGroupNamePrefix(logGroup);

		Optional<LogGroup> logGroupOptional = awsLogsClient.describeLogGroups(describeLogGroupsRequest).getLogGroups()
				.stream().filter(logGroup1 -> logGroup1.getLogGroupName().equals(logGroup)).findFirst();

		if (!logGroupOptional.isPresent()) {
			CreateLogGroupRequest createLogGroupRequest = new CreateLogGroupRequest().withLogGroupName(logGroup);
			awsLogsClient.createLogGroup(createLogGroupRequest);
		}

		DescribeLogStreamsRequest describeLogStreamsRequest = new DescribeLogStreamsRequest().withLogGroupName(logGroup)
				.withLogStreamNamePrefix(logStream);

		Optional<com.amazonaws.services.logs.model.LogStream> logStreamOptional = awsLogsClient
				.describeLogStreams(describeLogStreamsRequest).getLogStreams().stream()
				.filter(logStream1 -> logStream1.getLogStreamName().equals(logStream)).findFirst();

		if (!logStreamOptional.isPresent()) {
			CreateLogStreamRequest createLogStreamRequest = new CreateLogStreamRequest().withLogGroupName(logGroup)
					.withLogStreamName(logStream);
			awsLogsClient.createLogStream(createLogStreamRequest);
		}

	}

	/**
	 * Check the logginhEventQueue is empty or not if not then send the message to
	 * cloud-watch.
	 * 
	 */
	public void close() {
		while (loggingEventsQueue != null && !loggingEventsQueue.isEmpty()) {
			this.sendMessages();
		}
		keepDaemonActive.set(false);
	}

	public boolean requiresLayout() {
		return true;
	}

	/**
	 * Get the region for cloud-watch.
	 * 
	 * @return
	 */
	private Regions getAwsRegion() {

		if (this.region.isPresent()) {
			return Regions.fromName(this.region.get());
		} else {
			// Try first for an environment variable
			String configuredDefaultRegion = System.getenv("AWS_DEFAULT_REGION");
			if (configuredDefaultRegion != null) {
				return Regions.fromName(configuredDefaultRegion);
			}

			// Try and get our current region...
			com.amazonaws.regions.Region region = Regions.getCurrentRegion();
			if (region != null) {
				return Regions.fromName(region.getName());
			}
		}

		// Default to us-east-1
		return Regions.US_EAST_1;
	}

	/**
	 * AWSLogsClient is created based on region.
	 * 
	 */
	public void activateOptions() {
		if (isBlank(logGroup) || isBlank(logStream)) {
			this.close();
		} else {
			this.awsLogsClient = AWSLogsClientBuilder.standard().withRegion(this.getAwsRegion()).build();
			loggingEventsQueue = new LinkedBlockingQueue<>(queueLength);
			try {
				initializeCloudwatchResources();
				keepDaemonActive.set(true);
				initCloudwatchDaemon();
				cloudwatchAppenderInitialised.set(true);
			} catch (Exception e) {
				if (DEBUG_MODE) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Initialized the cloud-watch demon thread and also send the message to
	 * cloud-watch
	 * 
	 */
	private void initCloudwatchDaemon() {
		Thread t = new Thread(() -> {
			while (keepDaemonActive.get()) {
				if (!loggingEventsQueue.isEmpty()) {
					sendMessages();
				}
				try {
					Thread.currentThread();
					Thread.sleep(20L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				;
			}
		});
		t.setName(CustomLog4jAppender.class.getSimpleName() + " daemon thread");
		t.setDaemon(true);
		t.start();
	}

	/**
	 * Check the LogGroup value and logStream value is null or not.
	 * 
	 * @param string
	 * @return
	 */
	private boolean isBlank(String string) {
		return null == string || string.trim().length() == 0;
	}
}
