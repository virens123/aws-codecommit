package com.abbott.adc.service.businessevent.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class containing Execution details for logging
 *
 */
public class Execution {
	
	@JsonProperty("Timestamp")
	private String timestamp;
	@JsonProperty("LogID")
	private String logID;
	@JsonProperty("LoggingMessage")
	private String loggingMessage;
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getLogID() {
		return logID;
	}
	public void setLogID(String logID) {
		this.logID = logID;
	}
	public String getLoggingMessage() {
		return loggingMessage;
	}
	public void setLoggingMessage(String loggingMessage) {
		this.loggingMessage = loggingMessage;
	}
	
	

}
