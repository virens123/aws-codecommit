package com.abbott.adc.service.businessevent.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class containing log structure for logging
 *
 */
public class Log {
	
	@JsonProperty("LOG")
	private LogStructure logStructure;

	public LogStructure getLogStructure() {
		return logStructure;
	}

	public void setLogStructure(LogStructure logStructure) {
		this.logStructure = logStructure; 
	}
	
	

}
