package com.abbott.adc.service.businessevent.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *Domain Object for the Events received from CSR.
 */
public class Event {

	//unique field as an identifier 
    @JsonProperty("eventID")
    private String eventID;
    
    //this field is used for grouping of messages
    @JsonProperty("messageGroupID")
    private String messageGroupID;
    
    //time at which the event is created
    @JsonProperty("eventCreationTime")
    private String eventCreationTime;

    //unique id sent with the event
    @JsonProperty("eventCorrelationID")
    private String eventCorrelationID;
    
    @JsonProperty("entityKeys")
    private List<EntityKey> entityKeys = null;
    @JsonProperty("entityAttributes")
    private List<EntityAttribute> entityAttributes = null;
	public String getEventID() {
		return eventID;
	}
	public void setEventID(String eventID) {
		this.eventID = eventID;
	}
	public String getMessageGroupID() {
		return messageGroupID;
	}
	public void setMessageGroupID(String messageGroupID) {
		this.messageGroupID = messageGroupID;
	}
	public String getEventCreationTime() {
		return eventCreationTime;
	}
	public void setEventCreationTime(String eventCreationTime) {
		this.eventCreationTime = eventCreationTime;
	}
	public String getEventCorrelationID() {
		return eventCorrelationID;
	}
	public void setEventCorrelationID(String eventCorrelationID) {
		this.eventCorrelationID = eventCorrelationID;
	}
	public List<EntityKey> getEntityKeys() {
		return entityKeys;
	}
	public void setEntityKeys(List<EntityKey> entityKeys) {
		this.entityKeys = entityKeys;
	}
	public List<EntityAttribute> getEntityAttributes() {
		return entityAttributes;
	}
	public void setEntityAttributes(List<EntityAttribute> entityAttributes) {
		this.entityAttributes = entityAttributes;
	}

    
}