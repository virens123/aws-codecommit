package com.abbott.adc.service.businessevent.util;

import org.springframework.stereotype.Component;

import com.abbott.adc.service.businessevent.constants.EventConstants;

@Component
public class EventTopicUtil {

	/*
	 * setting topic value on the basis of eventOriginationSystem
	 */

	public String createTopic(String eventOriginationSystem, String eventType) {
		return EventConstants.TOPICPREFIX.getVal() + eventOriginationSystem + "." + eventType.replace("_EVENT", ".")
				+ EventConstants.TOPICSUFFIX.getVal();
	}
}
