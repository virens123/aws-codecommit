package com.abbott.adc.service.businessevent.model;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 *Domain Object containing Entity Attributes
 */
public class EntityAttribute {

	// This field contains the EntityAttribute name
    @JsonProperty("name")
    private String name;
    
	// This field contains the EntityAttribute value
    @JsonProperty("value")
    private String value;
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
    
    
}