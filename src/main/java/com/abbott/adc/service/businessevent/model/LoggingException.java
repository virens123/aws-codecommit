package com.abbott.adc.service.businessevent.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * class containing exception details used for logging
 *
 */
public class LoggingException {

	@JsonProperty("ErrorType")
	private String errorType;
	@JsonProperty("ErrorText")
	private String errorText;
	@JsonProperty("ErrorCode")
	private String errorCode;
	public String getErrorType() {
		return errorType;
	}
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	public String getErrorText() {
		return errorText;
	}
	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	
}
