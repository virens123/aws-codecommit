package com.abbott.adc.service.businessevent.publisher;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jms.core.JmsTemplate;

import com.abbott.adc.service.businessevent.model.EntityAttribute;
import com.abbott.adc.service.businessevent.model.EntityKey;
import com.abbott.adc.service.businessevent.model.Event;
import com.abbott.adc.service.businessevent.util.LoggingUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

@ExtendWith(MockitoExtension.class)
public class EventPublisherTest {
	
	@InjectMocks
	EventPublisher eventPublisher;
	
	@Mock
	JmsTemplate jmsTemplate;
	
	@Mock
	LoggingUtil loggingUtil;
	
	static Event event = new Event();
	
	@BeforeAll
	static void setUp() {
		EntityAttribute attribute = new EntityAttribute();
		List<EntityAttribute> listAttribute = new ArrayList<EntityAttribute>();
		List<EntityKey> listKey = new ArrayList<EntityKey>();
		EntityKey key = new EntityKey();
		
		key.setName("name");
		key.setValue("value");
		listKey.add(key);
		
		attribute.setName("name");
		attribute.setValue("value");		
		listAttribute.add(attribute);
		event.setEventCorrelationID(String.valueOf(UUID.randomUUID()));
		event.setEventCreationTime("10012018:06:30:11");
		event.setEventID("E1001");
		event.setMessageGroupID("1001");
		event.setEntityAttributes(listAttribute);
		event.setEntityKeys(listKey);
		
	}
	
	@Test
	public void testSend() throws JsonProcessingException {
		eventPublisher.send("ABBOTT.ADC.B2C.CUSTOMER.VTOPIC", event, "India", "EN", "B2C_CUSTOMER_CREATE_EVENT", "1001", "B2C","ADC_CUSTOMER","CUSTOMER_EVENT");
	}
	
}
