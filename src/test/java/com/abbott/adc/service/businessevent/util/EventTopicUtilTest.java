package com.abbott.adc.service.businessevent.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class EventTopicUtilTest {

	@InjectMocks
	EventTopicUtil eventTopicUtil;

	@Test
	public void testCreateTopic() {
		String topic = eventTopicUtil.createTopic("B2C", "CUSTOMER_EVENT");
		assertEquals("ABBOTT.ADC.B2C.CUSTOMER.TOPIC", topic);
	}

}
