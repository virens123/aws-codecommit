package com.abbott.adc.service.businessevent.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.abbott.adc.service.businessevent.model.EntityAttribute;
import com.abbott.adc.service.businessevent.model.EntityKey;
import com.abbott.adc.service.businessevent.model.Event;
import com.abbott.adc.service.businessevent.publisher.EventPublisher;
import com.abbott.adc.service.businessevent.util.EventTopicUtil;
import com.abbott.adc.service.businessevent.util.LoggingUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

@ExtendWith(MockitoExtension.class)
public class EventApiControllerTest {

	@InjectMocks
	EventApiController eventApiController;

	@Mock
	EventPublisher eventPublisher;
	
	@Mock
	LoggingUtil loggingUtil;
	
	@Mock
	EventTopicUtil eventTopicUtil;
	
	static Event event = new Event();
 
	@BeforeAll
	static void setUp() {
		EntityAttribute attribute = new EntityAttribute();
		List<EntityAttribute> listAttribute = new ArrayList<EntityAttribute>();
		List<EntityKey> listKey = new ArrayList<EntityKey>();
		EntityKey key = new EntityKey();
		
		key.setName("name");
		key.setValue("value");
		listKey.add(key);
		
		attribute.setName("name");
		attribute.setValue("value");		
		listAttribute.add(attribute);
		event.setEventCorrelationID(String.valueOf(UUID.randomUUID()));
		event.setEventCreationTime("10012018:06:30:11");
		event.setEventID("E1001");
		event.setMessageGroupID("1001");
		event.setEntityAttributes(listAttribute);
		event.setEntityKeys(listKey);
		
	}

	@Test
	public void testProcessEvents() throws JsonProcessingException{
		ResponseEntity<Object> entity = eventApiController.processEvents("India", "EN", "1001", "24311", "ADC_CUSTOMER", "CUSTOMER_EVENT", "B2C_CUSTOMER_CREATE_EVENT", "B2C", event);
		assertEquals(200, entity.getStatusCodeValue());

	}
	
	@Test
	public void testManageEventsJsonExceptionFromPublisher() throws JsonProcessingException {
		when(eventTopicUtil.createTopic("B2C", "CUSTOMER_EVENT")).thenReturn("ABBOTT.ADC.B2C.CUSTOMER.VTOPIC");
		doThrow(JsonProcessingException.class).when(eventPublisher).send("ABBOTT.ADC.B2C.CUSTOMER.VTOPIC", event, "India", "EN", "B2C_CUSTOMER_CREATE_EVENT", "1001", "B2C","ADC_CUSTOMER","CUSTOMER_EVENT");
		Assertions.assertThrows(JsonProcessingException.class, () -> {
			eventApiController.processEvents("India", "EN", "1001", "24311", "ADC_CUSTOMER", "CUSTOMER_EVENT", "B2C_CUSTOMER_CREATE_EVENT", "B2C", event);
		    });
	}
}
